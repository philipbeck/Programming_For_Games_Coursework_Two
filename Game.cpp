#include <iostream>
#include <vector>
#include <time.h>
#include "Shape.h"
#include "Circle.h"
#include "Square.h"

using namespace std;

//all of these constants can be changed to give the game different properties
//max and min values for the shapes to be in
const float MAX_X = 120;
const float MAX_Y = 120;
const float MIN_X = 0;
const float MIN_Y = 0;
const int SHAPE_NUMBER = 20;
const float MAX_SIZE = 25;
const float MIN_SIZE = 15;
const float MAX_SPEED = 10;

int main(){
	vector<Shape*> shapes;

	//seed the random number generator
	srand(time(NULL));

	int round;
	string input;
	while (true) {

		cout << "+++++SHAPES+++++\n";
		//randomly fill the vector with squares and made according to th constants
		for(int i = 0; i < SHAPE_NUMBER; i++){

			cout << i << ": ";

			if(rand()%2){
				shapes.push_back(new Circle(randomFloat(MIN_X,MAX_X),randomFloat(MIN_Y,MAX_Y), "Circle " + std::to_string(i) ,randomFloat(MIN_SIZE,MAX_SIZE) / 2));
				cout << shapes[i]->getName() <<  " radius: " << ((Circle*)shapes[i])->getRadius();
			}
			else{
				shapes.push_back(new Square(randomFloat(MIN_X,MAX_X),randomFloat(MIN_Y,MAX_Y), "Square " + std::to_string(i) ,randomFloat(MIN_SIZE,MAX_SIZE)));
				cout << shapes[i]->getName() << " side: " << ((Square*)shapes[i])->getSide();
			}

			cout << " X: " << shapes[i]->getX() << " Y: " << shapes[i]->getY() << endl;
		}

		cout << "\n";


		round = 1;
		while (shapes.size() > 1) {
			cout << "Round " << round << "\n";
			//first loop moves all of the shapes
			for (Shape* s : shapes) {
				//first move the shape randomly
				s->move(MAX_SPEED);
				//then if it goes past an edge loop it to the other side
				//first for x
				if (s->getX() > MAX_X) {
					s->setX((s->getX() - MAX_X) + MIN_X);
				}
				else if (s->getX() < MIN_X) {
					s->setX((s->getX() - MIN_X) + MAX_X);
				}
				//then for y
				if (s->getY() > MAX_Y) {
					s->setY((s->getY() - MAX_Y) + MIN_Y);
				}
				else if (s->getY() < MIN_Y) {
					s->setY((s->getX() - MIN_Y) + MAX_Y);
				}
			}

			//check for collisions
			for (int i = 1; i < shapes.size(); i++) {
				for (int j = i - 1; j >= 0; j--) {
					if (shapes[i]->getCollision(*shapes[j])) {
						cout << "Collision between:\n" << shapes[i]->getName() << " at (" << shapes[i]->getX() << "," << shapes[i]->getY()
							<< ") and " << shapes[j]->getName() << " at (" << shapes[j]->getX() << "," << shapes[j]->getY() << ")\n";
						shapes[i]->setToDestroy(true);
						shapes[j]->setToDestroy(true);
					}
				}
			}

			//then destroy and delete all the shapes that collided
			for (int i = 0; i < shapes.size(); i++) {
				//while instead of if since a different shape will be in i everytime it is destroyed
				while (i < shapes.size() && shapes[i]->getToDestroy()) {
					//delete it first to avoid memory leaks etc.
					delete shapes[i];
					shapes.erase(shapes.begin() + i);
				}
			}

			round++;
		}

		if (shapes.size() == 1) {
			cout << shapes[0]->getName() << " wins!\n";
			//delete the last shape
			delete shapes[0];
			shapes.erase(shapes.begin());
		}
		else {
			cout << "No shape won this game\n";
		}

		//code for people to play the game again
		cout << "\nEnter \"QUIT\" to finish or anything else to play again: ";
		cin >> input;

		if (input == "QUIT" || input == "quit") {
			break;
		}
	}

	return 0;
}