#include "Circle.h"

//constructors
Circle::Circle(): Shape(){
	radius = 0;
	boundingBox = Square(getX() - radius, getY()-radius, "box", radius * 2);
}

Circle::Circle(float x, float y, std::string name, float radius): Shape(x,y,name){
	this->radius = radius;
	boundingBox = Square(getX() - radius, getY()-radius, "box", radius * 2);
}

Circle::Circle(const Circle& circle){
	setX(circle.getX());
	setY(circle.getY());
	radius = circle.getRadius();
	boundingBox = Square();
}

Square Circle::getBoundingBox(){
	boundingBox.setX(getX() - radius);
	boundingBox.setY(getY() - radius);
	return boundingBox;
}

//doing stuff
bool Circle::pythagoras(float radius,float x, float y) const{
	//return two if they haven't hit
	return (pow(this->radius + radius, 2) > (pow(getX() - x, 2) + pow(getY() - y, 2)));
}


bool Circle::getCollision(Shape& shape) {
	if(shape.getClass() == ShapeType::CIRCLE){

		//you can't cast a shape to a circle so loads of referencing needs to be done
		Circle circle = *(Circle*)&shape;
		if (getBoundingBox().getCollision(circle.getBoundingBox())) {
			if(pythagoras(circle.getRadius(), circle.getX(), circle.getY())){
				return true;
			}
		}
	}
	else{
		Square square = *(Square*)&shape;
		if(getBoundingBox().getCollision(shape)){
			if (getX() > (square.getX() - radius) && getX() < (square.getX() + square.getSide() + radius) && getY() > square.getY() && getY() < (square.getY() + square.getSide())) {
				return true;
			}
			else if (getY() > (square.getY() - radius) && getY() < (square.getY() + square.getSide() + radius) && getX() > square.getX() && getX() < (square.getX() + square.getSide())) {
				return true;
			}
			else if (pythagoras(0,square.getX(), square.getY()) || pythagoras(0,square.getX() + square.getSide(), square.getY())
				|| pythagoras(0,square.getX(), square.getY() + square.getSide()) || pythagoras(0,square.getX() + square.getSide(), square.getY() + square.getSide()) ){
				return true;
			}
		}
	}

	//if there wasn't a collision return false
	return false;
}