#include "Square.h"

//constructors
Square::Square(): Shape(){
	side = 0;
}

Square::Square(float x, float y, std::string name, float side): Shape(x,y,name){
	this->side = side;
}

Square::Square(const Square& square){
	setX(square.getX());
	setY(square.getY());
	side = square.getSide ();
}

//doing 
bool Square::getCollision(Shape& shape){
	if(shape.getClass() == ShapeType::SQUARE){
		Square square(*(Square*)&shape);
		//draws a bigger square where if the corner of this is in within it then the two squares are overlapping
		if ((square.getX() - side) < getX() && (square.getY() - side) < getY() && (square.getX() + square.getSide()) > getX() && (square.getY() + square.getSide()) > getY()) {
			return true;
		}
	}
	else{
		//uses circle's collision test to save code duplication
		return shape.getCollision(Square(*this));
	}

	return false;
}
