#pragma once

#include <string>
#include <stdlib.h>

float randomFloat(float a, float b);

enum ShapeType{CIRCLE, SQUARE};

class Shape{
private:
	float position[2];//position[0] = x position[1] = 
	bool toDestroy;//to tell the game whether the Shape needs destroying or not
	std::string name;
public:

	//constructors
	Shape();
	Shape(float x, float y, std::string name);
	//getters and setters
	inline float getX() const { return position[0]; }
	inline void setX(float x) { position[0] = x; }
	inline float getY() const { return position[1]; }
	inline void setY(float y) { position[1] = y; }
	inline bool getToDestroy() const { return toDestroy; }
	inline void setToDestroy(bool toDestroy) {this->toDestroy = toDestroy; }
	inline std::string getName() { return name; }
	//for working out what kind of shape it is
	virtual inline ShapeType getClass() const = 0;
	//doing things
	//takes in a float for how much the shape can move
	void move(float);
	virtual bool getCollision(Shape& shape) = 0;

};