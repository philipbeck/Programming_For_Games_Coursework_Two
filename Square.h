#pragma once

#include "Shape.h"

class Square: public Shape{
private:
	float side;
public:
	//constructors
	Square();
	Square(float x, float y, std::string name, float side);
	Square(const Square& square);
	//gettters and setters
	inline float getSide() const { return side; }
	inline ShapeType getClass() const { return ShapeType::SQUARE; }
	//doing stuff
	bool getCollision(Shape& shape);
};