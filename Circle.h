#pragma once

#ifndef CIRCLE_H
#define CIRCLE_H

#include <math.h>

#include "Shape.h"
//included here rather than the cpp since circle wont be included in square
#include "Square.h"

class Circle: public Shape{
private:
	float radius;
	//returns true if two circles intersect
	bool pythagoras(float radius, float x, float y) const;
	Square boundingBox;
public:
	//constructors
	Circle();
	Circle(float x, float y, std::string name, float radius);
	Circle(const Circle& circle);
	//gettters and setters
	inline float getRadius() const { return radius; }
	inline ShapeType getClass() const { return ShapeType::CIRCLE; }
	Square getBoundingBox();
	//doing stuff
	bool getCollision(Shape& shape);
};

#endif