#include "Shape.h"


//generates a random float between a and b
float randomFloat(float a, float b) {
	int max;
	int min;
	float n;
	if (a > b) {
		max = a;
		min = b;
	}
	else {
		max = b;
		min = a;
	}

	n = rand() / (float)RAND_MAX;

	n *= (max - min);
	n += min;

	return n;
}

//SHAPE STUFF++++++++++++++++++++++++++++++++++++++++
//costrucors
Shape::Shape(){
	setX(0);
	setY(0);
	toDestroy = false;
}

Shape::Shape(float x, float y, std::string name){
	setX(x);
	setY(y);
	this->name = name;
	toDestroy = false;
}

//doing stuff
//move takes in float dist as the maximum distance that can be moved on each axis
void Shape::move(float dist){
	setX(getX() + randomFloat(-dist,dist));
	setY(getY() + randomFloat(-dist,dist));
}